import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WikipediaListComponent } from './wikipedia-list.component';

describe('WikipediaListComponent', () => {
  let component: WikipediaListComponent;
  let fixture: ComponentFixture<WikipediaListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WikipediaListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WikipediaListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
