import { Component, OnInit, SecurityContext } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { debounceTime, distinct, flatMap, tap } from 'rxjs/operators';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

import { WikipediaService } from '../services/wikipedia.service';

@Component({
  selector: 'app-wikipedia-list',
  templateUrl: './wikipedia-list.component.html',
  styleUrls: ['./wikipedia-list.component.css']
})
export class WikipediaListComponent implements OnInit {

  results: Array<string>;
  queryTerm = new FormControl('gorgonzola')
  pageText: SafeHtml;
  debounceIndicatorFull: boolean;


  constructor(private wikiServe: WikipediaService, public domSanitizer: DomSanitizer) {

    this.queryTerm.valueChanges
      .pipe(
        tap(
          () => {
            this.debounceIndicatorReset();
          }
        ),
        debounceTime(500),
        distinct(),
        flatMap(
          term => this.wikiServe.search(term)
        )
      ).subscribe(v => {
        this.results = v;
      });
  }

  ngOnInit() {
    this.getSearchData('gorgonzola');
  }

  getSearchData(queryString) {
    this.wikiServe.search(queryString).subscribe(v => {
      this.results = v;
    });

  }

  getPageData(queryString) {
    this.wikiServe.getPage(queryString).subscribe(v => {
      this.pageText = v.text["*"];

      //this.pageText = this.domSanitizer.sanitize(SecurityContext.HTML, v.text["*"]);
    });

  }


  debounceIndicatorReset(): void {
    this.debounceIndicatorFull = false;
    setTimeout(() => { this.debounceIndicatorFull = true; }, 100
    );

  }



}
