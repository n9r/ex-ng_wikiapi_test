import { Injectable } from '@angular/core';
import { URLSearchParams, Jsonp } from '@angular/http';
import { map } from 'rxjs/operators';

@Injectable()
export class WikipediaService {

  constructor(private jsonp: Jsonp) { }


  // DOCUMENTATION https://www.mediawiki.org/wiki/API
  search(term: string) {
    let search = new URLSearchParams()
    search.set('action', 'opensearch');
    search.set('search', term);
    search.set('format', 'json');
    return this.jsonp
      .get('http://en.wikipedia.org/w/api.php?callback=JSONP_CALLBACK', { search })
      // .toPromise()
      // .then(response => response.json()[1]);
      .pipe(map(response => response.json()[1]));
  }

  // example response: https://en.wikipedia.org/w/api.php?action=parse&page=test
  // https://en.wikipedia.org/w/api.php?format=xml&action=query&prop=extracts&titles=Stack%20Overflow&redirects=true
  getPage(pageTitle: string) {
    let search = new URLSearchParams()
    search.set('action', 'parse');
    search.set('page', pageTitle);
    search.set('format', 'json');
    return this.jsonp
      .get('http://en.wikipedia.org/w/api.php?callback=JSONP_CALLBACK', { search })
      .pipe(map(response => {
        return response.json().parse;
      }
      ));
  }



}