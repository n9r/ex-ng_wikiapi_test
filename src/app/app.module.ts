import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { HttpModule, JsonpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { WikipediaListComponent } from './wikipedia-list/wikipedia-list.component';

import { WikipediaService } from './services/wikipedia.service';


@NgModule({
  declarations: [
    AppComponent,
    WikipediaListComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    JsonpModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [WikipediaService],
  bootstrap: [AppComponent]
})
export class AppModule { }
